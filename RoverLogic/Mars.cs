﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RoverLogic.Objects;

namespace RoverLogic
{
    public class Mars : ISurface
    {
        private int[,] area;
        public Mars(Dimension dimension)
        {
            area = new int[dimension.Width, dimension.Height];
            InitializeArea();
        }

        public int GetXBoundary()
        {
            return area.GetLength(0);
        }

        public int GetYBoundary()
        {
            return area.GetLength(1);
        }

        public void MoveOne(CardinalPoint cardinalPoint)
        {
            Location currentLocation = GetMarkerLocation();
            if (currentLocation != null)
            {

                area[currentLocation.X, currentLocation.Y] = 0;
                switch (cardinalPoint)
                {
                    case CardinalPoint.N:
                        if (currentLocation.Y - 1 >= 0)
                            currentLocation.Y--;
                        break;
                    case CardinalPoint.S:
                        if (currentLocation.Y + 1 < GetYBoundary())
                            currentLocation.Y++;
                        break;
                    case CardinalPoint.E:
                        if (currentLocation.X + 1 < GetXBoundary())
                            currentLocation.X++;
                        break;
                    case CardinalPoint.W:
                        if (currentLocation.X - 1 >= 0)
                            currentLocation.X--;
                        break;
                }
                area[currentLocation.X, currentLocation.Y] = 1;
            }
            else
            {
                throw new Exception("No Location Marker has been set, first set a marker location.");
            }
        }

        public void RemoveMarkerLocation()
        {
            Location location = GetMarkerLocation();
            if(location != null)
                area[location.X, location.Y] = 0;
        }

        public Location GetMarkerLocation()
        {
            for (int x = 0; x < area.GetLength(0); x++)
            {
                for (int y = 0; y < area.GetLength(1); y++)
                {
                    if (area[x, y] == 1)
                        return new Location(x, y);
                }
            }

            return null;
        }

        public void SetMarkerLocation(Location location)
        {
            ValidationMessage validationMessage = ValidateLocation(location);
            if (validationMessage.Valid)
            {
                RemoveMarkerLocation();
                area[location.X, location.Y] = 1;
            }
            else
            {
                throw new Exception(validationMessage.Message);
            }
        }

        public ValidationMessage ValidateLocation(Location location)
        {
            ValidationMessage validationMessage = new ValidationMessage();
            validationMessage.Valid = true;
            if(location.X < 0 || location.X > GetXBoundary() || location.Y < 0 || location.Y > GetYBoundary())
            {
                validationMessage.Valid = false;
                validationMessage.Message = "The given location is outside the boundary of the area, please specify a valid location";
            }
            return validationMessage;
        }

        private void InitializeArea()
        {
            for(int x = 0; x < area.GetLength(0); x++)
            {
                for(int y = 0; y < area.GetLength(1); y++)
                {
                    area[x, y] = 0;
                }
            }
        }
    }
}
