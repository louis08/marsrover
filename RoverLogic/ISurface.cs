﻿using RoverLogic.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoverLogic
{
    public interface ISurface
    {
        int GetXBoundary();
        int GetYBoundary();
        void RemoveMarkerLocation();
        Location GetMarkerLocation();
        void SetMarkerLocation(Location location);
        ValidationMessage ValidateLocation(Location location);
        void MoveOne(CardinalPoint cardinalPoint);
    }
}
