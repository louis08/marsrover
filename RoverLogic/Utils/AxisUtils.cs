﻿using RoverLogic.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoverLogic.Utils
{
    public static class MovementUtils
    {
        public static Axis GetAxisFromCardinalPoint(CardinalPoint cardinalPoint)
        {
            Axis axis = Axis.X;
            switch (cardinalPoint)
            {
                case CardinalPoint.N:
                case CardinalPoint.S:
                    axis = Axis.Y;
                    break;
                case CardinalPoint.E:
                case CardinalPoint.W:
                    axis = Axis.X;
                    break;
            }

            return axis;
        }
    }
}
