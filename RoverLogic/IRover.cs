﻿using RoverLogic.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoverLogic
{
    public interface IRover
    {
        void Move(params MovementCommand[] movementCommands);
    }
}
