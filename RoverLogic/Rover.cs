﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RoverLogic.Objects;
using RoverLogic.Utils;

namespace RoverLogic
{
    public class Rover : IRover
    {
        private ISurface _surface;
        private Direction _direction;

        public Location CurrentLocation { get { return _surface.GetMarkerLocation(); } }
        public CardinalPoint CurrentCardianlPoint
        {
            get { return _direction.CurrentCardianlPoint; }
        }

        public RoverPosition CurrentRoverPosition
        {
            get { return new RoverPosition() { RoverCardinalPoint = CurrentCardianlPoint, RoverLocation = CurrentLocation }; }
        }

        public Rover(RoverPosition startingPostion, ISurface surface)
        {
            _surface = surface;
            _surface.SetMarkerLocation(startingPostion.RoverLocation);
            _direction = new Direction(startingPostion.RoverCardinalPoint);
        }

        public void Move(params MovementCommand[] movementCommands)
        {
            foreach(MovementCommand command in movementCommands)
            {
                switch (command)
                {
                    case MovementCommand.L:
                        _direction.MoveLeft();
                        break;
                    case MovementCommand.R:
                        _direction.MoveRight();
                        break;
                    case MovementCommand.M:
                        _surface.MoveOne(_direction.CurrentCardianlPoint);
                        break;
                }
            }
        }
    }
}
