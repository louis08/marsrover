﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoverLogic.Objects
{
    public class ValidationMessage
    {
        public bool Valid { get; set; }
        public string Message { get; set; }
    }
}
