﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoverLogic.Objects
{
    public class Direction
    {
        CardinalPoint currentCardianPoint;

        public CardinalPoint CurrentCardianlPoint
        {
            get { return currentCardianPoint; }
        }

        public Direction(CardinalPoint startingCardianlPoint)
        {
            currentCardianPoint = startingCardianlPoint;
        }

        public void MoveLeft()
        {
            currentCardianPoint = (CardinalPoint)CalculatePostion((int)currentCardianPoint, -1);
        }

        public void MoveRight()
        {
            currentCardianPoint = (CardinalPoint)CalculatePostion((int)currentCardianPoint, 1);
            
        }

        private int CalculatePostion(int newPosition, int movement)
        {
            newPosition += movement;
            if (newPosition > 4)
                newPosition = 1;
            else if (newPosition < 1)
                newPosition = 4;
            return newPosition;
        }
    }
}
