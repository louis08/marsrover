﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoverLogic.Objects
{
    public class RoverPosition
    {
        public Location RoverLocation { get; set; }

        public CardinalPoint RoverCardinalPoint { get; set; }

        public override string ToString()
        {
            return $"{RoverLocation.X + 1},{RoverLocation.Y + 1} {RoverCardinalPoint}";
        }
    }
}
