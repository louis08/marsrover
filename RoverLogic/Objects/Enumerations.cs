﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoverLogic.Objects
{
    /// <summary>
    /// E - East
    /// W - West
    /// N - North
    /// S - South
    /// </summary>
    public enum CardinalPoint
    {
        N = 1,
        E = 2,
        S = 3,
        W = 4
    }

    /// <summary>
    /// M - Move one space forward in the direction it is facing
    /// R - Rotate 90 degrees to the right
    /// L - Rotate 90 degrees to the left
    /// </summary>
    public enum MovementCommand
    {
        M,
        R,
        L
    }

    public enum Axis
    {
        X,
        Y
    }
}
