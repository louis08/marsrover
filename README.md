# MarsRover

# The application design 
	
	The application was created as a console application using the C# programing language with the .Net Core 2.2 framework for cross platform use.

# Application Correctness  
	
	NUnit unit testing framework was used to ensure Correctness and produce accurate results when using the application.
	
# Application Instructions
	Sends instructions to the Mars Rover

	Pass the following values to the Rover
			 The size of the Mars Surface as a number (8,8) this is a zone of 8x8 that contains 64 blocks
			 The starting position of the Rover as two numbers separated by a comma (1,2) where 1 = X axis and 2 = Y axis
			 The starting orientation of the rover as a single character E, W, N or S where E = East, W = West N = North and S = South
			 A list of commands to send to the Rover without any spaces MRL where M = Move one space forward, R - Rotate 90 degrees to the right, L - Rotate 90 Degrees to the left

	Example C:\MarsRover.exe 8,8 1,2 E MMLMRMMRRMML
