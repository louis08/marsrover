using NUnit.Framework;
using RoverLogic;
using RoverLogic.Objects;
using System;
using System.Collections.Generic;

namespace Tests
{
    public class Tests
    {
        private static object[] movementCommandList = 
        {
            new object[] {
                new MovementCommand[] { MovementCommand.M, MovementCommand.M, MovementCommand.L, MovementCommand.M, MovementCommand.R, MovementCommand.M, MovementCommand.M, MovementCommand.R, MovementCommand.R, MovementCommand.M, MovementCommand.M, MovementCommand.L },
                new RoverPosition() {RoverCardinalPoint = CardinalPoint.S, RoverLocation = new Location(2, 0) }
                },
            new object[] {
                new MovementCommand[] { MovementCommand.L, MovementCommand.M, MovementCommand.M, MovementCommand.M, MovementCommand.L, MovementCommand.L, MovementCommand.M, MovementCommand.L, MovementCommand.R, MovementCommand.M, MovementCommand.M, MovementCommand.R },
                new RoverPosition() {RoverCardinalPoint = CardinalPoint.W, RoverLocation = new Location(0, 3)}
            },
            new object[] {
                new MovementCommand[] { MovementCommand.R, MovementCommand.M, MovementCommand.M, MovementCommand.M, MovementCommand.R, MovementCommand.R, MovementCommand.M, MovementCommand.L, MovementCommand.R, MovementCommand.M, MovementCommand.M, MovementCommand.R },
                new RoverPosition() {RoverCardinalPoint = CardinalPoint.E, RoverLocation = new Location(0, 1)}
            },
            new object[] {
                new MovementCommand[] { MovementCommand.M, MovementCommand.L, MovementCommand.R, MovementCommand.M, MovementCommand.M, MovementCommand.M, MovementCommand.M, MovementCommand.R, MovementCommand.R, MovementCommand.L, MovementCommand.M, MovementCommand.L },
                new RoverPosition() {RoverCardinalPoint = CardinalPoint.E, RoverLocation = new Location(5, 2)}
            }
        };

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        [TestCaseSource("movementCommandList")]
        public void TestMovementList(MovementCommand[] movementCommands, RoverPosition resultRoverPosition)
        {
            Mars mars = new Mars(new Dimension() { Width = 14, Height = 14 });
            RoverPosition roverPosition = new RoverPosition();
            roverPosition.RoverCardinalPoint = CardinalPoint.E;
            roverPosition.RoverLocation = new Location(0, 1);
            Rover rover = new Rover(roverPosition, mars);

            rover.Move(movementCommands);
            Assert.AreEqual(resultRoverPosition.ToString(), rover.CurrentRoverPosition.ToString());
        }

        [Test]
        public void TestRotateAndMove()
        {
            Mars mars = new Mars(new Dimension() { Width = 14, Height = 14 });
            RoverPosition roverPosition = new RoverPosition();
            roverPosition.RoverCardinalPoint = CardinalPoint.N;
            roverPosition.RoverLocation = new Location(0, 0);
            Rover rover = new Rover(roverPosition, mars);

            rover.Move(MovementCommand.R, MovementCommand.M);
            Location newLocation = rover.CurrentLocation;
            Assert.AreEqual(1, newLocation.X);
        }

        [Test]
        public void TestEastBoundryWall()
        {
            Mars mars = new Mars(new Dimension() { Width = 14, Height = 14 });
            MovementCommand[] movementCommands =
            {
                MovementCommand.R,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
            };
            RoverPosition roverPosition = new RoverPosition();
            roverPosition.RoverCardinalPoint = CardinalPoint.N;
            roverPosition.RoverLocation = new Location(0, 0);
            Rover rover = new Rover(roverPosition, mars);

            rover.Move(movementCommands);
            Location newLocation = rover.CurrentLocation;
            Assert.AreEqual(13, newLocation.X);
        }

        [Test]
        public void TestSouthBoundryWall()
        {   
            Mars mars = new Mars(new Dimension() { Width = 14, Height = 14 });
            MovementCommand[] movementCommands =
            {
                MovementCommand.R,
                MovementCommand.R,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
                MovementCommand.M,
            };
            RoverPosition roverPosition = new RoverPosition();
            roverPosition.RoverCardinalPoint = CardinalPoint.N;
            roverPosition.RoverLocation = new Location(0, 0);
            Rover rover = new Rover(roverPosition, mars);

            rover.Move(movementCommands);
            Location newLocation = rover.CurrentLocation;
            Assert.AreEqual(13, newLocation.Y);
        }

        [Test]
        public void Test360Spin()
        {
            Mars mars = new Mars(new Dimension() { Width = 14, Height = 14 });
            MovementCommand[] movementCommands =
            {
                MovementCommand.R,
                MovementCommand.R,
                MovementCommand.R,
                MovementCommand.R
            };
            RoverPosition roverPosition = new RoverPosition();
            roverPosition.RoverCardinalPoint = CardinalPoint.N;
            roverPosition.RoverLocation = new Location(0, 0);
            Rover rover = new Rover(roverPosition, mars);

            rover.Move(movementCommands);
            Location newLocation = rover.CurrentLocation;
            Assert.AreEqual(CardinalPoint.N, rover.CurrentCardianlPoint);
        }

        [Test]
        public void TestOutOfBoundsPostion()
        {
            Assert.Throws<Exception>(new TestDelegate(PlaceRoverOutOfBounds));
        }

        private void PlaceRoverOutOfBounds()
        {
            try
            {
                Mars mars = new Mars(new Dimension() { Width = 14, Height = 14 });
                RoverPosition roverPosition = new RoverPosition();
                roverPosition.RoverCardinalPoint = CardinalPoint.N;
                roverPosition.RoverLocation = new Location(15, 15);
                Rover rover = new Rover(roverPosition, mars);
            }
            catch
            {
                throw;
            }
        }
    }
}