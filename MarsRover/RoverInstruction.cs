﻿using MarsRover.Utils;
using RoverLogic;
using RoverLogic.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsRover
{
    public class RoverInstruction
    {
        Mars mars;
        Rover rover;
        Dimension dimension;
        RoverPosition roverPosition;
        List<MovementCommand> movementCommands;

        public RoverInstruction()
        {
        }

        public RoverPosition SendCommands(string[] marsRoverCommands)
        {
            try
            {
                RoverPosition newRoverPosition = new RoverPosition();
                ValidationMessage validationMessage;
                if (ValidateInput(marsRoverCommands, out validationMessage))
                {
                    InitializeRover();
                    rover.Move(movementCommands.ToArray());
                    newRoverPosition.RoverLocation = rover.CurrentLocation;
                    newRoverPosition.RoverCardinalPoint = rover.CurrentCardianlPoint;
                }
                else
                {
                    throw new Exception($"Error validating input - {validationMessage.Message}");
                }
                return newRoverPosition;
            }
            catch
            {
                throw;
            }
        }

        private void InitializeRover()
        {
            try
            {
                mars = new Mars(dimension);
                rover = new Rover(roverPosition, mars);
            }
            catch
            {
                throw;
            }
        }

        private bool ValidateInput(string[] marsRoverCommands, out ValidationMessage validationMessage)
        {
            try
            {
                validationMessage = InputValidation.ValidateSurfaceDimension(marsRoverCommands[0].Trim());
                if (validationMessage.Valid)
                {
                    dimension = marsRoverCommands[0].ToDimension();
                    validationMessage = InputValidation.ValidateStartingPosition(marsRoverCommands[1].Trim());
                    if (validationMessage.Valid)
                    {
                        roverPosition = new RoverPosition();
                        roverPosition.RoverLocation = marsRoverCommands[1].ToLocation();
                        validationMessage = InputValidation.ValidateOrientation(marsRoverCommands[2].Trim());
                        if (validationMessage.Valid)
                        {
                            CardinalPoint cardinalPoint;
                            if(marsRoverCommands[2].ToCardinalPoint(out cardinalPoint))
                            {
                                roverPosition.RoverCardinalPoint = cardinalPoint;

                                validationMessage = InputValidation.ValidateCommands(marsRoverCommands[3].Trim());
                                if (validationMessage.Valid)
                                {
                                    movementCommands = marsRoverCommands[3].ToCommandList();
                                }
                            }
                        }
                    }
                }
                return validationMessage.Valid;
            }
            catch
            {
                throw;
            }
        }

    }
}
