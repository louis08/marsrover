﻿using RoverLogic.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MarsRover.Utils
{
    public static class InputValidation
    {
        public static ValidationMessage ValidateSurfaceDimension(string dimension)
        {
            ValidationMessage validationMessage = new ValidationMessage();
            validationMessage.Valid = false;
            try
            {
                if (dimension.Contains(","))
                {
                    string[] listValues = dimension.Split(",");
                    if (listValues.Length == 2)
                    {
                        int width, height;
                        if (listValues[0].TryToPositiveInt(out width))
                        {
                            if (listValues[1].TryToPositiveInt(out height))
                                validationMessage.Valid = true;
                        }
                    }
                }

                if (!validationMessage.Valid)
                    validationMessage.Message = "The zone size is not in the correct format, it should be two non negative numbers separated by a comma e.g (8,8)";

            }
            catch (Exception e)
            {
                validationMessage.Message = e.Message;
            }
            return validationMessage;
        }

        public static ValidationMessage ValidateStartingPosition(string startingPosition)
        {
            ValidationMessage validationMessage = new ValidationMessage();
            validationMessage.Valid = false;
            try
            {
                if (startingPosition.Contains(","))
                {
                    string[] listValues = startingPosition.Split(",");
                    if (listValues.Length == 2)
                    {
                        int x, y;
                        if (listValues[0].TryToPositiveInt(out x))
                        {
                            if (listValues[1].TryToPositiveInt(out y))
                                validationMessage.Valid = true;
                        }
                    }
                }

                if (!validationMessage.Valid)
                    validationMessage.Message = "The starting position is not in the correct format, it should be two non negative numbers separated by a comma e.g (1,2)";
            }
            catch (Exception e)
            {
                validationMessage.Message = e.Message;
            }
            return validationMessage;
        }

        public static ValidationMessage ValidateOrientation(string orientation)
        {
            ValidationMessage validationMessage = new ValidationMessage();
            validationMessage.Valid = false;
            try
            {
                if(orientation.Trim().Length == 1)
                {
                    CardinalPoint cardinalPoint;
                    if(orientation.ToCardinalPoint(out cardinalPoint))
                    {
                        validationMessage.Valid = true;
                    }
                }

                if (!validationMessage.Valid)
                    validationMessage.Message = "The starting orientation of the rover as a single character E, W, N or S where E = East, W = West N = North and S = South";
            }
            catch (Exception e)
            {
                validationMessage.Message = e.Message;
            }
            return validationMessage;
        }

        public static ValidationMessage ValidateCommands(string commands)
        {
            ValidationMessage validationMessage = new ValidationMessage();
            validationMessage.Valid = false;
            try
            {
                validationMessage.Valid = Regex.IsMatch(commands.Trim().ToUpper(), "[MRL]+");
                if (!validationMessage.Valid)
                    validationMessage.Message = "A list of commands to send to the Rover without any spaces MRL where M = Move one space forward, R - Rotate 90 degrees to the right, L - Rotate 90 Degrees to the left";
            }
            catch (Exception e)
            {
                validationMessage.Message = e.Message;
            }
            return validationMessage;
        }
    }
}
