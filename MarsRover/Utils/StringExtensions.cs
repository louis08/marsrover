﻿using RoverLogic.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsRover.Utils
{
    public static class StringExtensions
    {
        public static bool TryToPositiveInt(this string value, out int result)
        {
            try
            {
                result = 0;
                bool isValid = false;
                if (Int32.TryParse(value, out result))
                {
                    if (result < 0)
                        result = result / -1;
                    isValid = true;
                }

                return isValid;
            }
            catch
            {
                throw;
            }
        }

        public static Dimension ToDimension(this string value)
        {
            try
            {
                string[] size = value.Split(',');
                int width, height;
                size[0].TryToPositiveInt(out width);
                size[1].TryToPositiveInt(out height);
                return new Dimension()
                {
                    Width = width,
                    Height = height
                };
            }
            catch
            {
                throw;
            }
        }

        public static Location ToLocation(this string value)
        {
            try
            {
                string[] size = value.Split(',');
                int x, y;
                size[0].TryToPositiveInt(out x);
                size[1].TryToPositiveInt(out y);
                return new Location(x - 1, y - 1);
            }
            catch
            {
                throw;
            }
        }


        public static bool ToCardinalPoint(this string orientation, out CardinalPoint cardinalPoint)
        {
            try
            {
                bool isValid = false;
                cardinalPoint = new CardinalPoint();
                object objCardinalPoint = Enum.Parse(typeof(CardinalPoint), orientation.ToUpper());
                if(objCardinalPoint != null)
                {
                    cardinalPoint = (CardinalPoint)objCardinalPoint;
                    isValid = true;
                }
                return isValid;
            }
            catch
            {
                throw;
            }
        }

        public static List<MovementCommand> ToCommandList(this string commands)
        {
            try
            {
                List<MovementCommand> movementCommands = new List<MovementCommand>();
                foreach(char command in commands)
                {
                    movementCommands.Add(Enum.Parse<MovementCommand>(command.ToString().ToUpper()));
                }

                return movementCommands;
            }
            catch
            {
                throw;
            }
        }
    }
}
