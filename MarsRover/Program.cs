﻿using System;

namespace MarsRover
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 4)
            {
                PrintHelp();
            }
            else
            {
                try
                {
                    RoverInstruction roverInstruction = new RoverInstruction();
                    Console.WriteLine(roverInstruction.SendCommands(args).ToString());
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        public static void PrintHelp()
        {
            Console.WriteLine("Sends instructions to the Mars Rover");
            Console.WriteLine("\r\nPass the following values to the Rover");
            Console.WriteLine("\t The size of the Mars Surface as a number (8,8) this is a zone of 8x8 that contains 64 blocks");
            Console.WriteLine("\t The starting position of the Rover as two numbers separated by a comma (1,2) where 1 = X axis and 2 = Y axis");
            Console.WriteLine("\t The starting orientation of the rover as a single character E, W, N or S where E = East, W = West N = North and S = South");
            Console.WriteLine("\t A list of commands to send to the Rover without any spaces MRL where M = Move one space forward, R - Rotate 90 degrees to the right, L - Rotate 90 Degrees to the left");
            Console.WriteLine("");
            Console.WriteLine("Example C:\\MarsRover.exe 8,8 1,2 E MMLMRMMRRMML");
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
